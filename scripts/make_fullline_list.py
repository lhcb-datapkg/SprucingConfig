###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
    Produces list of FULL lines for TISTOS persistency from the HLT2 job opts
    Update hlt2optsdumpfile, outputfile and run with (note the Moore version is inconsequential here)

    ```
    mkdir -p fake-siteroot/DBASE/SprucingConfig
    ln -s $PWD fake-siteroot/DBASE/SprucingConfig/v999999999
    CMAKE_PREFIX_PATH=$PWD/fake-siteroot:$CMAKE_PREFIX_PATH lb-run --use SprucingConfig.v999999999 Moore/v55r10p1 python scripts/make_fullline_list.py
    ```
"""

#This needs to point to the job opts of the HLT2 step you are interested in
hlt2optsdumpfile="/eos/lhcb/user/a/abertoli/lhcb/spruce/SprucingConfig/job_hlt2_2024.json"

outputdir="SprucingConfig/Spruce24/fullline_config/"
#This needs to be a suitably named outputfile for the new campaign
outputfile="pp_Collision24c4.py"

from helperfunctions import get_hlt2_streaming

#Lumi line not to be streamed to disk
omit_lines = ["Hlt2Lumi"]



def main():
    decisions=get_hlt2_streaming(hlt2optsdumpfile)

    full_hlt2_lines = [
        line for line in decisions['full']
        if not any(x in line for x in omit_lines)
    ]

    f = open(outputdir+outputfile, "w")
    f.write("full_lines =" + str(full_hlt2_lines))
    f.close()

    print(f"Created {outputfile} in {outputdir}")


if __name__ == '__main__':
    main()
