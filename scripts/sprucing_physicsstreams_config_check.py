###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
    Verifies whether all the HLT2 TURCAL and TURBO lines are to be streamed in passthrough Sprucing and the HLT2 FULL lines are to be included for TISTOS persistency in exclusive Sprucing

    Update hlt2optsdumpfile and the SprucingConfig file and run with (note the Moore version is inconsequential here)

    ```
    CMAKE_PREFIX_PATH=$PWD/fake-siteroot:$CMAKE_PREFIX_PATH lb-run --use SprucingConfig.v999999999 Moore/vXXrY python scripts/sprucing_physicsstreams_config_check.py
    ```
"""
import re


#This needs to point to the job opts of the HLT2 step you are interested in
hlt2optsdumpfile="/afs/cern.ch/work/n/ngrieser/Sprucing/SprucingConfig/job_hlt2.json"

#This needs to point to the campaign options you are interested in
from SprucingConfig.Spruce24.Sprucing_production_physics_pp_Collision24c4 import (
    turbolinedict, nominal_lines, persist_reco_lines, rawbank_lines,
    persist_reco_rawbank_lines, full_lines_for_TISTOS)

from scripts.helperfunctions import get_hlt2_streaming

def main():
    #Lumi line not to be streamed to disk
    omit_lines = ["Hlt2Lumi"]
    #Monitoring and Commissioning lines not to be streamed to disk
    turcal_omit_lines = ["Hlt2Monitoring", "Hlt2DQ", "Hlt2CalibMon"] + omit_lines

    ##Get HLT2 lines by stream ("Decision" suffix added for TURBO for regex compatibility)
    decisions = get_hlt2_streaming(hlt2optsdumpfile)
    turbo_hlt2_lines = [
        line + "Decision" for line in decisions["turbo"]
        if not any(x in line for x in omit_lines)
    ]
    turcal_hlt2_lines = [
        line for line in decisions['turcal']
        if not any(x in line for x in turcal_omit_lines)
    ]
    full_hlt2_lines = [
        line for line in decisions['full']
        if not any(x in line for x in omit_lines)
    ]

    ##Add together all WG regexes
    turbo_regex = [
        item for sublist in [list(turbolinedict[wg]) for wg in turbolinedict]
        for item in sublist
    ]
    r = re.compile('|'.join(turbo_regex))
    ###print(f"r : {r.pattern}")

    #TURBO
    ##Turbo lines to be spruced are
    turbo_spruce_lines = list(filter(r.match, turbo_hlt2_lines))
    print(f"{len(turbo_spruce_lines)} turbo lines to be spruced")
    ##Turbo lines running in HLT2 but not being streamed by Sprucing are
    turbo_hlt2_not_spruce = list(set(turbo_hlt2_lines) - set(turbo_spruce_lines))
    
    #TURCAL
    ##Turcal lines to be spruced are
    turcal_spruce_lines = nominal_lines + persist_reco_lines + rawbank_lines + persist_reco_rawbank_lines
    print(f"{len(turcal_spruce_lines)} turcal lines to be spruced")
    ##Turcal lines running in HLT2 but not being streamed by Sprucing are
    turcal_hlt2_not_spruce = list(
        set(turcal_hlt2_lines) - set(turcal_spruce_lines))
    ##Turcal lines set to be Spruced but not running in HLT2 are
    turcal_spruce_not_hlt2 = list(
        set(turcal_spruce_lines) - set(turcal_hlt2_lines))

    #FULL
    ##Full lines for TISTOS in exclusive sprucing are
    full_spruce_lines = full_lines_for_TISTOS
    ##Full lines running in HLT2 but not with TISTOS persistency in sprucing are
    full_hlt2_not_spruce = list(set(full_hlt2_lines) - set(full_spruce_lines))
    print(f"{len(full_spruce_lines)} full lines to be exclusively spruced")
    ##Full lines set for TISTOS persistency but not running in HLT2 are
    full_spruce_not_hlt2 = list(set(full_spruce_lines) - set(full_hlt2_lines))

    has_failed = False

    turbo = (turbo_hlt2_not_spruce, [])
    turcal = (turcal_hlt2_not_spruce, turcal_spruce_not_hlt2)
    full = (full_hlt2_not_spruce, full_spruce_not_hlt2)
    streams = {"TURBO": turbo, "TURCAL": turcal, "FULL": full}

    for stream, lines in streams.items():
        hlt2_not_spruce, spruce_not_hlt2 = lines
        if hlt2_not_spruce:
            print(
                f"ERROR:: Lines {hlt2_not_spruce} of {stream} stream will not be spruced and therefore will not be avaliable on disk. In the case of FULL lines you will not have TISTOS persistency. Please update the Sprucing configuration.\n"
            )
            has_failed = True

        if spruce_not_hlt2:
            print(
                f"ERROR:: Lines {spruce_not_hlt2} of {stream} stream are due to be spruced but did not run in HLT2. Please update the Sprucing configuration.\n"
            )
            has_failed = True
        if not has_failed:
            print(f"{stream} stream correctly configured \n")

    if has_failed:
        raise RuntimeError("Some lines failed one of the line tests.\n")

    print("SUCCESS:: All lines passed the tests.")


if __name__ == '__main__':
    main()
