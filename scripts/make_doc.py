##Usage : modify hlt2optsdumpfile and the Sprucing_production_physics_pp_Collision24c1 file to the ones you need
##run with : `lb-run --use SprucingConfig.vXXrYpZ Moore/vXXrY python make_doc.py -r <campaign name>`

import ast
from pathlib import Path
import json
import argparse
import re

#This needs to point to the job opts of the HLT2 step you are interested in
hlt2optsdumpfile="/eos/lhcb/user/a/abertoli/lhcb/spruce/SprucingConfig/job_hlt2_2024.json"

#This needs to point to the campaign options you are interesed in
from SprucingConfig.Spruce24.Sprucing_production_physics_pp_Collision24c2 import (
    turbolinedict, nominal_lines, persist_reco_lines, rawbank_lines,
    persist_reco_rawbank_lines, full_lines_for_TISTOS, fulllinedict)

#### Argument parser
parser = argparse.ArgumentParser(
    usage="./run python -i %(prog)s -r myprod",
    description='Inspect Moore output')
parser.add_argument(
    '-r', '--ref', type=str, help='Name of campaign', required=False)
args = parser.parse_args()

def get_hlt2_streaming(hlt2optsdumpfile):
    """Takes job opts dump of HLT2 job and returns the streaming configuration. Million thanks to Chris Burr for this!"""
    in_path=Path(hlt2optsdumpfile)
    app_config = {k: ast.literal_eval(v) for k, v in json.loads(in_path.read_text()).items()}
    decisions = dict()
    for name, _, nodes, _ in app_config["HLTControlFlowMgr.CompositeCFNodes"]:
        if not name.endswith("_writer"):
            continue
        if name in decisions:
            raise NotImplementedError("Found two nodes named '{name}'")
        if len(nodes) != 2 or not nodes[0].startswith(
                "HltDecReportsFilter/HltDecReportsFilter_"):
            raise NotImplementedError(f"Unexpected nodes for {name}: {nodes}")
        decisions[name.removesuffix('_writer')] = [
            line for line in app_config[nodes[0].split("/", 1)[1]+".Lines"]
        ]
    return decisions


decisions = get_hlt2_streaming(hlt2optsdumpfile)

#### Find turbo lines that ran which will be streamed in passthrough
turbo_lines_match = {wg: [] for wg in turbolinedict.keys()}

for wg in turbolinedict.keys():
    r = re.compile('|'.join(turbolinedict[wg]))
    turbo_lines_match[wg] = list(filter(r.match, decisions["turbo"]))

for wg in turbo_lines_match.keys():
    turbo_lines_match[wg]=[line.removesuffix('Decision') for line in turbo_lines_match[wg]]


print("\n Passthrough job has following streams ")
for wg in turbo_lines_match.keys():
    print(f"\n Stream {wg} will have : ", turbo_lines_match[wg])


#### Write this along with turcal and exclusive sprucing config to a file
f = open(f"config_for_{args.ref}.txt", "w")

f.write("\n==================\nTurbo passthrough job has following streams\n==================\n")
for wg in turbo_lines_match.keys():
    f.write(f"{wg.upper()}\n")
    f.write("======\n")
    for line in turbo_lines_match[wg]:
        f.write(f"    * {line}\n")

f.write("\n==================\nTurcal passthrough job has following streams\n==================\n")
f.write("Turcal mDST-like (no persistreco and no detector rawbanks) \n")
f.write("======\n")
for line in nominal_lines:
    f.write(f"    * {line}\n")

f.write("Turcal persistreco (persistreco, no detector rawbanks) \n")
f.write("======\n")
for line in persist_reco_lines:
    f.write(f"    * {line}\n")

f.write("Turcal detector rawbanks (detector rawbanks, no persistreco) \n")
f.write("======\n")
for line in rawbank_lines:
    f.write(f"    * {line}\n")

f.write("Turcal persistreco and detector rawbank  (persistreco and detector rawbanks) \n")
f.write("======\n")
for line in persist_reco_rawbank_lines:
    f.write(f"    * {line}\n")

f.write("\n==================\nExclusive Sprucing job has following streams\n==================\n")
f.write("======\n")
for wg in fulllinedict.keys():
    f.write(f"{wg.upper()}\n")
    f.write("======\n")
    for line in fulllinedict[wg]:
        f.write(f"    * {line}\n")


f.write("Exclusive sprucing will have TISTOS persistency for the following HLT2 FULL lines \n")
f.write("======\n")
for line in full_lines_for_TISTOS:
    f.write(f"    * {line}\n")

f.close()
