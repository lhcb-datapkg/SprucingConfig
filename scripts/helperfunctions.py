
### To hold various required functions for SprucingConfig
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import (make_odin, default_raw_banks, configure_input, configure)
from PyConf.reading import dstdata_filter
from Moore import moore_control_flow
from PyConf.Algorithms import (HltRoutingBitsFilter, LHCb__Tests__RunEventCountAlg)
from GaudiConf.LbExec import InputProcessTypes



def get_hlt2_streaming(hlt2optsdumpfile):
    """Takes job opts dump of HLT2 job and returns the streaming configuration. \n
    Million thanks to Chris Burr for this!
    """

    import ast
    from pathlib import Path
    import json

    in_path=Path(hlt2optsdumpfile)
    app_config = {k: ast.literal_eval(v) for k, v in json.loads(in_path.read_text()).items()}
    decisions = dict()
    for name, _, nodes, _ in app_config["HLTControlFlowMgr.CompositeCFNodes"]:
        if not name.endswith("_writer"):
            continue
        if name in decisions:
            raise NotImplementedError("Found two nodes named '{name}'")
        if len(nodes) != 2 or not nodes[0].startswith(
                "HltDecReportsFilter/HltDecReportsFilter_"):
            raise NotImplementedError(f"Unexpected nodes for {name}: {nodes}")
        decisions[name.removesuffix('_writer')] = [
            line.removesuffix("Decision")
            for line in app_config[nodes[0].split("/", 1)[1]+".Lines"]
        ]
    for name, lines in decisions.items():
        print(f"First 3 lines of stream {name} are ({len(lines)}) :",
              *lines[:3], "...")
    return decisions


def run_moore_nofilter(options,
              make_streams=None,
              public_tools=[]):

    """Redefine Moore's control and data flow to remove the physics bit filter before `moore_control_node`.
    """

    process = "pass"
    config = configure_input(options)
    streams = (make_streams or options.lines_maker)()

    moore_control_node = moore_control_flow(options, streams, process)

    #Filter to return true if physics bit 95 is "on" for this event
    rb_bank = default_raw_banks('HltRoutingBits')
    lumiFilterRequireMask = (0x0, 0x0, 0x40000000)

    lumi_filter = [
        HltRoutingBitsFilter(
            name="LumiFilter",
            RawBanks=rb_bank,
            RequireMask=lumiFilterRequireMask)
    ]

    lumi_sprucing_node = CompositeNode(
        'lumi_sprucing_node',
        combine_logic=NodeLogic.LAZY_AND,
        children=lumi_filter +
        [LHCb__Tests__RunEventCountAlg(name="LumiCounter", ODIN=make_odin())],
        force_order=True)

    spruce_control_node = CompositeNode(
        'spruce_control_node',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[lumi_sprucing_node] + [moore_control_node],
        force_order=False)

    config.update(configure(options, spruce_control_node, public_tools=public_tools))

    return config


def run_moore_DstDataFilter(options,
              make_streams=None,
              public_tools=[]):


    config = configure_input(options)
    # Then create the data (and control) flow for all streams.
    streams = (make_streams or options.lines_maker)()

    process = "spruce"

    # Combine all lines and output in a global control flow.
    moore_control_node = moore_control_flow(options, streams, process)

    # Filter to return true if physics bit 95 is "on" for this event
    rb_bank = default_raw_banks("HltRoutingBits")
    physFilterRequireMask = (0x0, 0x0, 0x80000000)
    lumiFilterRequireMask = (0x0, 0x0, 0x40000000)

    rb_filter = [
        HltRoutingBitsFilter(
            name="PhysFilter",
            RawBanks=rb_bank,
            RequireMask=physFilterRequireMask,
            PassOnError=False,
        )
    ]

    lumi_filter = [
        HltRoutingBitsFilter(
            name="LumiFilter",
            RawBanks=rb_bank,
            RequireMask=lumiFilterRequireMask,
            PassOnError=False,
        )
    ]

    dstdata_filtering=[dstdata_filter(source=InputProcessTypes(options.input_process).sourceID())]

    physics_sprucing_node = CompositeNode(
        "physics_sprucing_node",
        combine_logic=NodeLogic.LAZY_AND,
        children=rb_filter + dstdata_filtering + [moore_control_node],
        force_order=True,
    )

    lumi_sprucing_node = CompositeNode(
        "lumi_sprucing_node",
        combine_logic=NodeLogic.LAZY_AND,
        children=lumi_filter
        + [LHCb__Tests__RunEventCountAlg(name="LumiCounter", ODIN=make_odin())],
        force_order=True,
    )

    spruce_control_node = CompositeNode(
        "spruce_control_node",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[lumi_sprucing_node] + [physics_sprucing_node],
        force_order=False,
    )

    top_node = spruce_control_node


    config.update(configure(options, top_node, public_tools=public_tools))
    return config
