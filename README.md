# Data package containing Python code used by Sprucing productions

### Responsibles : @nskidmor, @abertoli, @eleckste, @shunan, @tizhou

Options files for running Sprucing productions separated by `physics` (to run on FULL, TURBO and TURCAL streams) and `tech` (to run on NOBIAS, HLT2CALIB and LUMI streams).

To run using a tagged SprucingConfig and Moore version

```
lb-run --use SprucingConfig.vXXrY Moore/vXXrY lbexec SprucingConfig.Spruce2X.Sprucing_production_<type>_pp_<data>2XcY:<function> <options>.yaml
```

If you have not tagged SprucingConfig yet but wish to try it then in the head `SprucingConfig/` directory do, for instance

```
mkdir -p fake-siteroot/DBASE/SprucingConfig
``````

```
ln -s $PWD fake-siteroot/DBASE/SprucingConfig/v999999999
```

```
CMAKE_PREFIX_PATH=$PWD/fake-siteroot:$CMAKE_PREFIX_PATH lb-run --use SprucingConfig.v999999999 Moore/vXXrY lbexec --export sprucingjob.opts SprucingConfig.Spruce24.Sprucing_production_physics_pp_Collision24c1:turbo_spruce_production <options>.yaml
```

(note also the `--dry-run` lbexec option if you just want to dump the job opts)

If done correctly you should be able to do

```
CMAKE_PREFIX_PATH=$PWD/fake-siteroot:$CMAKE_PREFIX_PATH lb-run --use SprucingConfig.v999999999 Moore/v55r4 python -c 'import SprucingConfig; print(SprucingConfig)'
```

and see that `SprucingConfig`` is imported from your local copy.