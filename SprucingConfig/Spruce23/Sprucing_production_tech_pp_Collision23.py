
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of technical streams (LUMI) on COLLISION23 data

from Moore import Options
from Moore.lines import PassLine
from Moore.streams import DETECTORS, Stream, Streams



from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import (make_odin, default_raw_banks, configure_input, configure)
from Moore import moore_control_flow
from PyConf.Algorithms import (HltRoutingBitsFilter, LHCb__Tests__RunEventCountAlg)

def run_moore_nofilter(options,
              make_streams=None,
              public_tools=[]):
    """Redefine Moore's control and data flow to remove the physics bit filter before `moore_control_node`.
    """
    process = "pass"
    config = configure_input(options)
    streams = (make_streams or options.lines_maker)()

    moore_control_node = moore_control_flow(options, streams, process)

    #Filter to return true if physics bit 95 is "on" for this event
    rb_bank = default_raw_banks('HltRoutingBits')
    lumiFilterRequireMask = (0x0, 0x0, 0x40000000)

    lumi_filter = [
        HltRoutingBitsFilter(
            name="LumiFilter",
            RawBanks=rb_bank,
            RequireMask=lumiFilterRequireMask)
    ]

    lumi_sprucing_node = CompositeNode(
        'lumi_sprucing_node',
        combine_logic=NodeLogic.LAZY_AND,
        children=lumi_filter +
        [LHCb__Tests__RunEventCountAlg(name="LumiCounter", ODIN=make_odin())],
        force_order=True)

    spruce_control_node = CompositeNode(
        'spruce_control_node',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[lumi_sprucing_node] + [moore_control_node],
        force_order=False)

    config.update(configure(options, spruce_control_node, public_tools=public_tools))

    return config


###################### PASSTHROUGH SPRUCING ON LUMI STREAM ####################

##Note: `Hlt2LumiCalibration` passes `Hlt1ODIN1kHzLumiDecision` and `Hlt2LumiDefaultRawBanks` passes `Hlt1ODINLumiDecision`

def lumi_spruce_production(options: Options):
    """Do not pass detector rawbanks"""
    def make_streams():
        streams = [
            Stream(
                "lumi",
                lines=[PassLine(name="Passlumi", )],
                detectors=DETECTORS),
        ]

        return Streams(streams=streams)


    config = run_moore_nofilter(options, make_streams, public_tools=[])
    for producer in config["HiveDataBrokerSvc/HiveDataBrokerSvc"].DataProducers:
        if producer.name() == "CombineRawBanks_for_lumi":
            producer.AllowSameSourceID = True
            break
    else:
        raise NotImplementedError("Failed to find CombineRawBanks_for_lumi")
    return config
