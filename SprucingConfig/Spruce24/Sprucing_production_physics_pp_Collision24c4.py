###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of physics streams (FULL, TURBO, TURCAL) on COLLISION24 data (c4)

from Configurables import HLTControlFlowMgr
from Moore import Options, run_moore
from Hlt2Conf.lines import sprucing_lines
from Moore.lines import PassLine, SpruceLine, optimize_controlflow

from PyConf.reading import get_particles, upfront_decoder
from Moore.streams import DETECTORS, Stream, Streams
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.monitoring import run_default_monitoring

from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.legacy_rec_hlt1_tracking import (make_VeloClusterTrackingSIMD)
from RecoConf.decoders import default_VeloCluster_source


from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.ift import sprucing_lines as spruce_ift_lines


allowed_failures=2

###################### EXCLUSIVE SPRUCING ON FULL STREAM ####################

fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    "ift": spruce_ift_lines,
}

## Following configures FULL HLT2 lines that need TISTOS info in exclusive Sprucing
from SprucingConfig.Spruce24.fullline_config.pp_Collision24c4 import full_lines as full_lines_for_TISTOS


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def make_excl_spruce_prod_streams():

    spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    
    streams = [
            Stream(
                wg,
                lines=[builder() for builder in fulllinedict[wg].values()],
                detectors=[]) for wg in fulllinedict
        ]

    return Streams(streams=streams)


def excl_spruce_production(options: Options):

    from DDDB.CheckDD4Hep import UseDD4Hep
    if UseDD4Hep:
        from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter
        TrackMasterExtrapolator.global_bind(
            ApplyMultScattCorr=False,
            ApplyEnergyLossCorr=False,
            ApplyElectronEnergyLossCorr=False)
        TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

    public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
    ]
 
    default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")
    make_VeloClusterTrackingSIMD.global_bind(
        algorithm=VeloRetinaClusterTrackingSIMD)
    
    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
 
    with list_of_full_stream_lines.bind(lines=full_lines_for_TISTOS), optimize_controlflow.bind(optimization="default"), run_default_monitoring.bind(run=False):
        config = run_moore(options, make_excl_spruce_prod_streams, public_tools=public_tools)
        return config

###################### PASSTHROUGH SPRUCING ON TURBO STREAM ####################

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        '^(?!.*Full)Hlt2BandQ.*Decision',
        '^(?!.*Full)Hlt2_JpsiToMuMu.*Decision',
        '^(?!.*Full)Hlt2_Psi2SToMuMu.*Decision'
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "charm": ["Hlt2Charm.*Decision", "Hlt2HadInt.*Decision"],
    "qee": ["^(?!.*Full)Hlt2QEE.*Decision"],
    "rd": ["^(?!.*Gamma.*Incl)Hlt2RD.*Decision"],
    "bnoc": ["^(?!.*Full)Hlt2BnoC.*Decision"],
    "ift": ["Hlt2IFTTurbo.*Decision"],
    "trackeff":
    ["Hlt2TurboVelo2Long.*Decision", "Hlt2TrackEff_.*_FullEventDecision"]
}


def make_turbo_spruce_prod_streams():

    streams = [
            Stream(
                wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turbolinedict[wg],
                    )
                ],
                detectors=DETECTORS) for wg in turbolinedict
        ]

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )
    return Streams(streams=streams)

def turbo_spruce_production(options: Options):
    
    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    config = run_moore(options, make_turbo_spruce_prod_streams, public_tools=[])
    return config


###################### MDST SPRUCING ON TURCAL STREAM ####################

## mDST like, no persistreco and no detector rawbanks streamed to disk

rawbank_lines = [
    "Hlt2PID_BToJpsiK_JpsiToMuMumTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToMuMupTagged_Detached",
    "Hlt2PID_JpsiToMuMumTagged_Detached",
    "Hlt2PID_JpsiToMuMupTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged",
    "Hlt2PID_DstToD0Pi_D0ToKPi",
    "Hlt2PID_L0ToPPi_LL_LowPT",
    "Hlt2PID_L0ToPPi_LL_MidPT",
    "Hlt2PID_L0ToPPi_LL_HighPT",
    "Hlt2PID_L0ToPPi_LL_VeryHighPT",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2KshortVeloLong_HighPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_LowPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_VeryHighPT",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortHard",
    "Hlt2TrackEff_TurCalVelo2Long_Kshort",
    "Hlt2TrackEff_TurCalVelo2Long_KshortSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVHard",
]

persist_reco_rawbank_lines = [
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Match",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_MuonProbe_VELO",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_ElectronProbe_VELO",
]

nominal_lines = [
    "Hlt2PID_BToJpsiK_JpsiToPpPmTagged",
    "Hlt2PID_BToJpsiK_JpsiToPmPpTagged",
    "Hlt2PID_DsToPhiPi_PhiToMuMupTagged_Detached",
    "Hlt2PID_DsToPhiPi_PhiToMuMumTagged_Detached",
    "Hlt2PID_DstToD0Pi_D0ToKPiPiPi",
    "Hlt2PID_KsToPiPi_LL",
    "Hlt2PID_LbToLmdJpsi_DD",
    "Hlt2PID_LbToLmdJpsi_LL",
    "Hlt2PID_LbToLcMuNu_LcToPKPi",
    "Hlt2PID_LcToLmdPi_DD",
    "Hlt2PID_LcToLmdPi_LL",
    "Hlt2PID_LbToLcPi_LcToPKPi",
    "Hlt2PID_LcToPKPi",
    "Hlt2PID_OmegaToL0K_L0ToPPi_LLL",
    "Hlt2PID_PhiToKK_Unbiased_Detached",
    "Hlt2PID_PhiToKmKpTagged_Detached",
    "Hlt2PID_KsToPiPi_DD",
    "Hlt2PID_L0ToPPi_DD_LowPT",
    "Hlt2PID_L0ToPPi_DD_MidPT",
    "Hlt2PID_OmegaToL0K_L0ToPPi_DDD",
    "Hlt2PID_L0ToPPi_DD_HighPT",
    "Hlt2PID_PhiToKpKmTagged_Detached",
    "Hlt2PID_L0ToPPi_DD_VeryHighPT",
    "Hlt2PID_SMOG2Dst2KPi",
    "Hlt2PID_SMOG2KS2PiPiDD",
    "Hlt2PID_SMOG2KS2PiPiLLHighPT",
    "Hlt2PID_SMOG2KS2PiPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiDD",
    "Hlt2PID_SMOG2Lambda02PPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiLLHighPT",
    "Hlt2PID_SMOG2PIDJpsi2MuMumTagged",
    "Hlt2PID_SMOG2PIDJpsi2MuMupTagged",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kmprobe",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kpprobe",
    "Hlt2PID_SMOG2Phi2kk_Kmprobe",
    "Hlt2PID_SMOG2Phi2kk_Kpprobe"] + [line for line in rawbank_lines if "Hlt2TrackEff_TurCalVelo2Long_Kshort" not in line]

persist_reco_lines = [
    "Hlt2PID_BdToKstG",
    "Hlt2PID_Bs2PhiG",
    "Hlt2PID_EtaMuMuG",
    "Hlt2PID_Dsst2DsG",
    "Hlt2PID_D2EtapPi",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Resolved",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Merged",
]




def make_turcal_spruceline(hlt2_linename, persist_reco=False, prescale=1.):
    filter = f"{hlt2_linename}Decision"
    location = f"/Event/HLT2/{hlt2_linename}/Particles"
    if prescale<1.:
        hlt2_linename=hlt2_linename+"_ps"
    spruce_linename = hlt2_linename.replace("Hlt2", "SpruceTurCal")
    print(f"{filter} {location} {spruce_linename}")
    with upfront_decoder.bind(source="Hlt2"):
        hlt2_particles = get_particles(location)
        turcal_spruce_line = SpruceLine(
            name=spruce_linename,
            hlt2_filter_code=filter,
            algs=[hlt2_particles],
            persistreco=persist_reco,
            prescale=prescale)
    return turcal_spruce_line


def make_turcal_spruce_prod_streams():

    streams = [
            Stream(
                "turcal_mdst",
                lines=[make_turcal_spruceline(line) for line in nominal_lines],
                detectors=[]),
            Stream(
                "turcal_persistreco",
                lines=[
                    make_turcal_spruceline(line, persist_reco=True)
                    for line in persist_reco_lines
                ],
                detectors=[]),
            Stream(
                "turcal_rawbanks",
                lines=[make_turcal_spruceline(line, prescale=0.4) for line in rawbank_lines],
                detectors=DETECTORS),
            Stream(
                "turcal_persistrecorawbanks",
                lines=[
                    make_turcal_spruceline(line, persist_reco=True)
                    for line in persist_reco_rawbank_lines
                ],
                detectors=DETECTORS)
        ]

    print(streams)
    return Streams(streams=streams)

    
def turcal_spruce_production(options: Options):
    
    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    with list_of_full_stream_lines.bind(lines=[]):
        config = run_moore(options, make_turcal_spruce_prod_streams, public_tools=[])
        return config
