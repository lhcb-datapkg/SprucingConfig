###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of physics streams (FULL, TURBO, TURCAL) on COLLISION24 data (c4)

from Configurables import HLTControlFlowMgr
from Moore import Options, run_moore
from Hlt2Conf.lines import sprucing_lines
from Moore.lines import optimize_controlflow

from Moore.streams import Stream, Streams
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.monitoring import run_default_monitoring

from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.legacy_rec_hlt1_tracking import (make_VeloClusterTrackingSIMD)
from RecoConf.decoders import default_VeloCluster_source


from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.ift import sprucing_lines as spruce_ift_lines


allowed_failures=2

###################### EXCLUSIVE SPRUCING ON FULL STREAM ####################

fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    "ift": spruce_ift_lines,
}

## Following configures FULL HLT2 lines that need TISTOS info in exclusive Sprucing
from SprucingConfig.Spruce24.fullline_config.pp_Collision24r1 import full_lines as full_lines_for_TISTOS


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def make_excl_spruce_prod_streams():

    spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )


    streams = [
            Stream(
                wg,
                lines=[builder() for builder in fulllinedict[wg].values()],
                detectors=[]) for wg in fulllinedict
        ]

    return Streams(streams=streams)


def excl_spruce_production(options: Options):

    from DDDB.CheckDD4Hep import UseDD4Hep
    if UseDD4Hep:
        from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter
        TrackMasterExtrapolator.global_bind(
            ApplyMultScattCorr=False,
            ApplyEnergyLossCorr=False,
            ApplyElectronEnergyLossCorr=False)
        TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

    public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
    ]

    default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")
    make_VeloClusterTrackingSIMD.global_bind(
        algorithm=VeloRetinaClusterTrackingSIMD)

    HLTControlFlowMgr().StopAfterNFailures = allowed_failures

    with list_of_full_stream_lines.bind(lines=full_lines_for_TISTOS), optimize_controlflow.bind(optimization="default"), run_default_monitoring.bind(run=False):
        config = run_moore(options, make_excl_spruce_prod_streams, public_tools=public_tools)
        return config
