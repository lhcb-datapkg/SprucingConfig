
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of technical streams (NOBIAS, HLT2CALIB, LUMI) on COLLISION24 data

from Configurables import HLTControlFlowMgr
from Moore import Options, run_moore
from Moore.lines import PassLine
from Moore.streams import DETECTORS, Stream, Streams
from scripts.helperfunctions import run_moore_nofilter

allowed_failures=5

###################### PASSTHROUGH SPRUCING ON NOBIAS STREAM ####################

#NoBias stream only has the HLT2NoBias line

def nobias_spruce_production(options: Options):
    """Pass all rawbanks"""
    def make_streams():
        streams = [
            Stream(
                "nobias",
                lines=[PassLine(name="Passnobias", hlt2_filter_code="Hlt2NoBias_NoBiasDecision")],
                detectors=DETECTORS),
        ]

        return Streams(streams=streams)

    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    config = run_moore(options, make_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON HLT2CALIB STREAM ####################

def hlt2calib_spruce_production(options: Options):
    """Pass all rawbanks from HLT2"""
    def make_streams():
        streams = [
            Stream(
                "hlt2calib",
                lines=[
                    PassLine(
                        name="Passhlt2calib"
                    )
                ],
                detectors=DETECTORS),
        ]

        return Streams(streams=streams)

    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    config = run_moore(options, make_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON LUMI STREAM ####################

##Note: `Hlt2LumiCalibration` passes `Hlt1ODIN1kHzLumiDecision` and `Hlt2LumiDefaultRawBanks` passes `Hlt1ODINLumiDecision`

def lumi_spruce_production(options: Options):
    """Do not pass detector rawbanks"""
    def make_streams():
        streams = [
            Stream(
                "lumi",
                lines=[PassLine(name="Passlumi", )],
                detectors=[]),
        ]

        return Streams(streams=streams)

    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    config = run_moore_nofilter(options, make_streams, public_tools=[])
    return config
