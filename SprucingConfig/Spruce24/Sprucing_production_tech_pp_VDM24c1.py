
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of technical streams (BEAMGAS, LUMI) on VDM24 data (c1)

from Moore import Options
from Moore.lines import PassLine
from Moore.streams import DETECTORS, Stream, Streams
from scripts.helperfunctions import run_moore_nofilter

###################### PASSTHROUGH SPRUCING ON LUMI STREAM ####################

##Note: `Hlt2LumiCalibration` passes `Hlt1ODIN1kHzLumiDecision` and `Hlt2LumiDefaultRawBanks` passes `Hlt1ODINLumiDecision`

def lumi_spruce_production(options: Options):
    """Pass all rawbanks and no physics filter"""
    def make_streams():
        streams = [
            Stream(
                "lumi",
                lines=[PassLine(name="Passlumi", )],
                detectors=DETECTORS),
        ]

        return Streams(streams=streams)

    config = run_moore_nofilter(options, make_streams, public_tools=[])
    return config

###################### PASSTHROUGH SPRUCING ON BEAMGAS STREAM ####################

def beamgas_spruce_production(options: Options):
    """Pass all rawbanks and include physics filter"""
    def make_streams():
        streams = [
            Stream(
                "beamgas",
                lines=[PassLine(name="Passbeamgas", )],
                detectors=DETECTORS),
        ]

        return Streams(streams=streams)

    config = run_moore_nofilter(options, make_streams, public_tools=[])
    return config