###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Patched option for concurrent Sprucing of TURBO stream on COLLISION24 data (name remained c3). 
## This removes the high BW line Hlt2QEE_MDS_BDT_nHits

from Moore import Options, run_moore
from Moore.lines import PassLine
from Moore.streams import DETECTORS, Stream, Streams

###################### PASSTHROUGH SPRUCING ON TURBO STREAM ####################

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        '^(?!.*Full)Hlt2BandQ.*Decision',
        '^(?!.*Full)Hlt2_JpsiToMuMu.*Decision',
        '^(?!.*Full)Hlt2_Psi2SToMuMu.*Decision'
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "charm": ["Hlt2Charm.*Decision", "Hlt2HadInt.*Decision"],
    "qee": ["^(?!.*Full)(?!.*Hlt2QEE_MDS_BDT_nHits)Hlt2QEE.*Decision"],
    "rd": ["^(?!.*Gamma.*Incl)Hlt2RD.*Decision"],
    "bnoc": ["^(?!.*Full)Hlt2BnoC.*Decision"],
    "ift": ["Hlt2IFTTurbo.*Decision"],
    "trackeff":
    ["Hlt2TurboVelo2Long.*Decision", "Hlt2TrackEff_.*_FullEventDecision"]
}


def make_turbo_spruce_prod_streams():

    streams = [
            Stream(
                wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turbolinedict[wg],
                    )
                ],
                detectors=DETECTORS) for wg in turbolinedict
        ]

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )
    return Streams(streams=streams)

def turbo_spruce_production(options: Options):
    config = run_moore(options, make_turbo_spruce_prod_streams, public_tools=[])
    return config