###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for concurrent Sprucing of PbPb data in 2024

from Configurables import HLTControlFlowMgr
from Moore import Options, run_moore
#from Hlt2Conf.lines import sprucing_lines
from Moore.lines import PassLine, SpruceLine#, optimize_controlflow
from scripts.helperfunctions import run_moore_DstDataFilter

from PyConf.reading import get_particles, upfront_decoder
from Moore.streams import Stream, Streams
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from PyConf.packing import persistreco_writing_version



#from Moore.monitoring import run_default_monitoring

#from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
#from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
#from RecoConf.legacy_rec_hlt1_tracking import (make_VeloClusterTrackingSIMD)
#from RecoConf.decoders import default_VeloCluster_source

allowed_failures=1
DETECTORS_Trimmed = ['UT', 'VP', 'Muon', 'FT']

###################### SPRUCING ON Ion-Raw (passthrough raw-bank+persistreco) ####################

ion_raw_lines = [
    "Hlt2PbPbDecision",
    "Hlt2PbPb_InclusiveDecision",
    ]

def make_turbo_prod_streams():

    stream = [Stream(
                "ionraw",
                lines=[
                    PassLine(
                        name="Pass_IonRaw",
                        hlt2_filter_code=ion_raw_lines,
                    )
                ],
                detectors=DETECTORS_Trimmed)]

    return Streams(streams=stream)

def turbo_production(options: Options):
    config = run_moore(options, make_turbo_prod_streams, public_tools=[])
    
    HLTControlFlowMgr().StopAfterNFailures = allowed_failures

    return config

###################### SPRUCING ON Ion STREAM ####################

# these lines has no persistreco but a few rawbanks for offline calibration
rawbank_lines = [
    "Hlt2TrackEff_SMOG2KshortVeloLong_LowPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_HighPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_VeryHighPT"
]

nominal_lines = [
    "Hlt2IFTNoReco_SMOG2D02pipi",
    "Hlt2IFTNoReco_SMOG2D02KK",
    "Hlt2IFTNoReco_SMOG2Etac2ppbar",
    "Hlt2IFTNoReco_SMOG2XicpTopKPi",
    "Hlt2IFTNoReco_SMOG2Xic0TopKKPi",
    "Hlt2IFTNoReco_SMOG2Omegac0TopKKPi",
    "Hlt2IFTNoReco_SMOG2ccTo4Pi",
    "Hlt2IFTNoReco_SMOG2ccTo2Pi2K",
    "Hlt2IFTNoReco_SMOG2ccTo4K",
    "Hlt2IFTNoReco_SMOG2Omega2Lambda0K_lll",
    "Hlt2IFTNoReco_SMOG2Xi2Lambda0pi_lll",
    "Hlt2PIDNoReco_SMOG2KS2PiPiLLLowPT",
    "Hlt2PIDNoReco_SMOG2KS2PiPiLLHighPT",
    "Hlt2PIDNoReco_SMOG2KS2PiPiDD",
    "Hlt2PIDNoReco_SMOG2Lambda02PPiLLLowPT",
    "Hlt2PIDNoReco_SMOG2Lambda02PPiLLHighPT",
    "Hlt2PIDNoReco_SMOG2Lambda02PPiDD",
    "Hlt2PIDNoReco_SMOG2Phi2kk_Kmprobe",
    "Hlt2PIDNoReco_SMOG2Phi2kk_Kpprobe",
    "Hlt2PIDNoReco_SMOG2Phi2kkHighPT_Kmprobe",
    "Hlt2PIDNoReco_SMOG2Phi2kkHighPT_Kpprobe",
]

persist_reco_lines = [
    "Hlt2IFTWithReco_SMOG2D02KPi",
    "Hlt2IFTWithReco_SMOG2Dpm2kpipi",
    "Hlt2IFTWithReco_SMOG2DsToKKPi",
    "Hlt2IFTWithReco_SMOG2LcTopKPi",
    "Hlt2IFTWithReco_SMOG2Detached2Body",
    "Hlt2IFTWithReco_SMOG2Detached3Body",
    "Hlt2IFTWithReco_SMOG2LowDiMuon",
    "Hlt2IFTWithReco_SMOG2LowDiMuonSS",
    "Hlt2IFTWithReco_SMOG2Jpsi2MuMu",
    "Hlt2IFTWithReco_SMOG2Jpsi2MuMuSS",
    "Hlt2IFTWithReco_SMOG2Ups2MuMu",
    "Hlt2IFTWithReco_SMOG2Ups2MuMuSS",
    "Hlt2IFTWithReco_SMOG2DY2MuMu",
    "Hlt2IFTWithReco_SMOG2DY2MuMuSS",
    "Hlt2IFTWithReco_SMOG2DY2MuMuExcludeCCBarLow",
    "Hlt2IFTWithReco_SMOG2DY2MuMuExcludeCCBarIntermediate",
    "Hlt2IFTWithReco_SMOG2DY2MuMuExcludeCCBarHigh",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Match",
    "Hlt2IFTWithReco_SMOG2SingleTrackHighPT",
    "Hlt2PID_SMOG2PIDJpsi2MuMupTagged",
    "Hlt2PID_SMOG2PIDJpsi2MuMumTagged",
]

passthrough_reco_lines = [
    "Hlt2PbPbUPC",
    "Hlt2PbSMOG_Hadronic",
    "Hlt2PbSMOG_MinBiasPassthrough",
]

def make_pass_line(hlt2_linename, persist_reco=False, pvntracks=False):
    filter = f"{hlt2_linename}Decision"
    location = f"/Event/HLT2/{hlt2_linename}/Particles"
    pass_linename = hlt2_linename.replace("Hlt2", "Spruce")
    print(f"{filter} {location} {pass_linename}")
    with upfront_decoder.bind(source="Hlt2"):
        hlt2_particles = get_particles(location)
        pass_line = SpruceLine(
            name=pass_linename,
            hlt2_filter_code=filter,
            algs=[hlt2_particles],
            persistreco=persist_reco,
            pv_tracks=pvntracks)
    return pass_line

def make_pass_reco_only_line(hlt2_linename):
    filter = f"{hlt2_linename}Decision"
    pass_linename = hlt2_linename.replace("Hlt2", "Spruce")
    print(f"{filter} {pass_linename}")
    with upfront_decoder.bind(source="Hlt2"):
        pass_line = SpruceLine(
            name=pass_linename,
            hlt2_filter_code=filter,
            algs=[],
            persistreco=True)
    return pass_line


def make_pass_prod_streams():

    streams = [
            Stream(
                "ionnoreco",
                lines=[make_pass_line(line,pvntracks=True) for line in nominal_lines],
                detectors=[]),
            Stream(
                "ionreco",
                lines=[
                    make_pass_line(line, persist_reco=True)
                    for line in persist_reco_lines
                ],
                detectors=[]),
            Stream(
                "ioncalib",
                lines=[make_pass_line(line) for line in rawbank_lines],
                detectors=DETECTORS_Trimmed),
            Stream(
                "ionpass",                
                lines=[make_pass_reco_only_line(line) for line in passthrough_reco_lines])
        ]

    ionstreams=Streams(streams=streams)
    ionstreams.print()
    return ionstreams

    
def pass_production(options: Options):
    
    HLTControlFlowMgr().StopAfterNFailures = allowed_failures
    
    with list_of_full_stream_lines.bind(lines=[]), persistreco_writing_version.bind(version=1.1):
        config = run_moore_DstDataFilter(options, make_pass_prod_streams, public_tools=[])
        return config
