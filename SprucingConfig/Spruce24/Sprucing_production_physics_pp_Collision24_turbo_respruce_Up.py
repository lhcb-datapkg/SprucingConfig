# Run locally
# ./run lbexec SprucingConfig.Spruce24.Sprucing_production_physics_pp_Collision24_turbo_respruce_Up:spruce_overlap resprucetestoptions.yaml >& log &

from Moore import Options
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.production import turbo_spruce
from scripts.helperfunctions import run_moore_DstDataFilter
from pathlib import Path
import json

def spruce_overlap(options: Options):
    """
    For resprucing the 2024 TURBO stream
    See https://gitlab.cern.ch/lhcb-dpa/project/-/issues/346
    """
    if options.input_run_number is None:
        raise ValueError("options.input_run_number is not set but is required")

    data_map = {

              ## bkk path = Beam6800GeV-VeloClosed-MagUp
              ## Configs below confirmed and validation output file on run 308321 checked by Nicole
              # Need 3 productions
              "S24c2Up" : {"runs":[301325, 304604], "version" : "v55r11p1"}, #staged
              "S24c3Up" : {"runs":[304648, 305739], "version" : "v55r12p3"},
              "S24c4Up" : {"runs":[308104, 308540], "version" : "v55r13p2"}, #staged

    }


    ## Connect the run number to the job
    for job in data_map.keys():
        if (options.input_run_number >= data_map[job]["runs"][0] and options.input_run_number <= data_map[job]["runs"][1]):
            print(f"For run {options.input_run_number} we run {job}")
            job_name = job
            break
    else:
        raise Exception(f"Run number {options.input_run_number} not found in data_map")

    # Open and merge the line_config and the streaming_config
    stream = "turbo"
    stream_config = Path(__file__).parent / "turboline_configs/turbo24_streaming_config.json"
    input_line_config = Path(__file__).parent / f"turboline_configs/MooreDev_{data_map[job_name]['version']}.json"

    with open(stream_config, "r") as json_file:
        streaming_config = json.load(json_file)
    with open(input_line_config, "r") as json_file:
        line_config = json.load(json_file)[stream]

    for line, l_config in line_config.items():
        for stream, s_config in streaming_config.items():
            if line in s_config:
                l_config["stream"] = stream
                #print(f"Line {line} is in stream {stream}")
                break
        else:
            raise Exception(f"Line {line} not found in any stream")


    custom_prescales = {}


    ## S24c3Up has "issues"
    if options.input_run_number<305558:
       custom_prescales = {"Hlt2QEE_MDS_BDT_nHits": 0.1}
       print(f"Input run number is {options.input_run_number} so setting custom prescales to {custom_prescales}")

       if job_name=="S24c3Up":
        to_remove=["Hlt2IFTTurbo_HypertritonToHePi_DD_VeloMatch", "Hlt2IFTTurbo_HypertritonToHePi_SS_LL", "Hlt2IFTTurbo_Lb0ToHePmPm", "Hlt2IFTTurbo_HypertritonToHePi_LL"]
        for line in to_remove:
            print(f"Removing line {line}")
            line_config.pop(line)

    def make_streams():
        return turbo_spruce(
            line_config, custom_prescales=custom_prescales, use_regex=False
        )

    with list_of_full_stream_lines.bind(lines=[]):
        config = run_moore_DstDataFilter(options, make_streams, public_tools=[])
        # Moore/v56r2 doesn't work with options.compression so we need to set it manually
        config['Gaudi::RootCnvSvc/RootCnvSvc'].GlobalCompression = "ZSTD:1"
        config['Gaudi::RootCnvSvc/RootCnvSvc'].MaxBufferSize = 1048576
        # It looks like a significant minority of files contain one unprocessable event
        config["HLTControlFlowMgr/HLTControlFlowMgr"].StopAfterNFailures = 1
        return config
